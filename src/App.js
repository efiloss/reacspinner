import React, { Component } from 'react';
import Main from './components/Main';
import Loader from './components/Loader';

import './App.css';

class App extends Component {

  state = {
    loading: true
  }

  loadingPage = () => {
    setTimeout(() => {
      this.setState({ loading: false });
    }, 4000);
  }

  render() {
    const { loading } = this.state;
    this.loadingPage();
    return (
      <div className="App" >
        {loading && <Loader />}
        {!loading && <Main />}
      </div>
    );
  }
}

export default App;
